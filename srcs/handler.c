/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 00:37:11 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/19 03:45:06 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void	ft_explore_sub(t_ls *env, t_dir *dir)
{
	t_dir	*tmp;

	while (dir->dirs)
	{
		ft_handle_dir(env, dir->dirs);
		tmp = dir->dirs;
		dir->dirs = dir->dirs->next;
		ft_strdel(&tmp->name);
		free(tmp->stat);
		free(tmp);
	}
}

void	ft_explore_dir(t_ls *env, t_dir *dir)
{
	t_dir	*tmp;

	if (dir->files)
		dir->files = ft_sort_files_alpha(dir->files);
	if (env->t)
		ft_sort_files_time(dir->files);
	if (env->r)
		dir->files = ft_sort_files_rev(dir->files);
	if (env->l)
		ft_print_dir_l(env, dir);
	else
		ft_print_dir(env, dir);
	tmp = dir->dirs;
	if (tmp)
		ft_sort_dirs_alpha(dir);
	if (tmp && env->t)
		ft_sort_dirs_time(dir);
	if (tmp && env->r)
		dir->dirs = ft_sort_dirs_rev(dir->dirs);
	ft_explore_sub(env, dir);
}

void	ft_handle_dir(t_ls *env, t_dir *dir)
{
	struct dirent	*tmp_dir;
	DIR				*dirp;
	char			*name;

	name = (S_ISLNK(dir->stat->st_mode) ? get_path(dir->name) : dir->name);
	if (!(dirp = opendir(name)))
	{
		ft_putstr_fd("ft_ls: ", 2);
		perror(dir->name);
		return ;
	}
	dir->files = NULL;
	while ((tmp_dir = readdir(dirp)) != NULL)
	{
		if (env->up_r)
		{
			if (tmp_dir->d_type == DT_DIR && !is_hidden(env, tmp_dir->d_name))
				ft_addir_path(dir, ft_strrjoin(name, "/", tmp_dir->d_name));
		}
		ft_addfile_from_dir(dir, tmp_dir);
	}
	ft_explore_dir(env, dir);
	closedir(dirp);
}

void	ft_handle_all(t_ls *env)
{
	t_dir		*dir;
	t_dir		*tmp;
	t_file		*file;
	t_format	*format;

	ft_sort_all(env);
	file = env->files;
	format = ft_fill_formats(file);
	while (file)
	{
		if (env->l)
			ft_print_file_l(env, ft_newdir(file->path), file, format);
		else
			ft_print_file(env, file);
		file = file->next;
	}
	dir = env->dirs;
	while (dir && (tmp = dir))
	{
		ft_handle_dir(env, dir);
		dir = dir->next;
		free(tmp->stat);
		free(tmp);
	}
	free(format);
}
