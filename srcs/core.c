/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 22:12:28 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/19 04:32:14 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void	ft_free_exit(char *reason, t_ls *env)
{
	ft_printt("%s\n", reason);
	free(env);
	exit(1);
}

void	ft_sort_all(t_ls *env)
{
	if (env->files && env->files->next)
		env->files = ft_sort_files_alpha(env->files);
	if (env->dirs && env->dirs->next)
		ft_sort_dirs_alpha(env->dirs);
	if (env->t)
	{
		if (env->files && env->files->next)
			ft_sort_files_time(env->files);
		if (env->dirs && env->dirs->next)
			ft_sort_dirs_time(env->dirs);
	}
	if (env->r)
	{
		if (env->files && env->files->next)
			env->files = ft_sort_files_rev(env->files);
		if (env->dirs && env->dirs->next)
			env->dirs = ft_sort_dirs_rev(env->dirs);
	}
}

int		main(int ac, char **av)
{
	t_ls	*env;

	if (!(env = malloc(sizeof(t_ls))))
		ft_free_exit("ft_ls: Can't allocate memory", env);
	env->dirs = NULL;
	env->files = NULL;
	env->up_r = 0;
	env->l = 0;
	env->a = 0;
	env->r = 0;
	env->t = 0;
	env->path = 0;
	ft_parse(env, ac, av);
	ft_handle_all(env);
	free(env);
	return (0);
}
