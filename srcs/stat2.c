/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stat2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 04:01:51 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/19 03:45:34 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void	print_link(t_dir *dir, t_file *file)
{
	char	result[256];
	int		ret;
	char	*name;

	name = ft_strrjoin(dir->name, "/", file->name);
	ret = readlink(name, result, 256);
	result[ret] = 0;
	ft_printt("%s -> %s\n", name, result);
	ft_strdel(&name);
}

size_t	ft_count_blocks(t_ls *env, t_dir *dir)
{
	size_t	count;
	t_file	*tmp;

	tmp = dir->files;
	count = 0;
	while (tmp)
	{
		if (is_hidden(env, tmp->name))
		{
			tmp = tmp->next;
			continue ;
		}
		count += tmp->stat->st_blocks;
		tmp = tmp->next;
	}
	return (count);
}

void	print_file_nlink(nlink_t st_nlink, size_t max)
{
	size_t	len;

	if (max != 2)
	{
		len = ft_nblen(st_nlink);
		max += 2;
		while (max > len++)
			ft_printt(" ");
	}
	else
		ft_printt("  ");
	ft_printt("%d ", st_nlink);
}

void	print_major_minor(dev_t st_rdev)
{
	size_t	min_len;
	size_t	maj_len;

	maj_len = ft_nblen(major(st_rdev));
	min_len = ft_nblen(minor(st_rdev));
	while (maj_len++ < 2)
		ft_printt(" ");
	ft_printt("%d,", major(st_rdev));
	while (min_len++ < 4)
		ft_printt(" ");
	ft_printt("%d ", minor(st_rdev));
}

void	print_file_size(off_t st_size, size_t max)
{
	size_t	len;

	len = ft_nblen(st_size);
	while ((max - len++) > 0)
		ft_printt(" ");
	ft_printt("%d ", st_size);
}
