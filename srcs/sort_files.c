/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_files.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 02:03:25 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/16 05:21:41 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void	ft_swap_files(t_file *file, t_file *next_file)
{
	void		*swap;
	struct stat *tmp_file;

	swap = file->name;
	file->name = next_file->name;
	next_file->name = swap;
	tmp_file = file->stat;
	file->stat = next_file->stat;
	next_file->stat = tmp_file;
	swap = file->path;
	file->path = next_file->path;
	next_file->path = swap;
}

t_file	*ft_sort_files_alpha(t_file *ret)
{
	t_file	*tmp;
	int		flag;

	flag = 1;
	if (!ret || !ret->next)
		return (NULL);
	while (flag)
	{
		tmp = ret;
		flag = 0;
		while (tmp->next)
		{
			if (ft_strcmp(tmp->name, tmp->next->name) > 0)
			{
				ft_swap_files(tmp, tmp->next);
				flag = 1;
			}
			tmp = tmp->next;
		}
	}
	return (ret);
}

void	ft_sort_files_time(t_file *ret)
{
	t_file	*tmp;
	int		flag;

	flag = 1;
	if (!ret || !ret->next)
		return ;
	while (flag)
	{
		tmp = ret;
		flag = 0;
		while (tmp->next)
		{
			if (tmp->stat->st_mtime < tmp->next->stat->st_mtime)
			{
				ft_swap_files(tmp, tmp->next);
				flag = 1;
			}
			tmp = tmp->next;
		}
	}
}

t_file	*ft_sort_files_rev(t_file *file)
{
	t_file *tmp1;
	t_file *tmp2;
	t_file *ret;

	ret = file;
	if (ret != NULL)
	{
		tmp1 = ret->next;
		ret->next = NULL;
		while (tmp1 != NULL)
		{
			tmp2 = tmp1->next;
			tmp1->next = ret;
			ret = tmp1;
			tmp1 = tmp2;
		}
	}
	return (ret);
}
