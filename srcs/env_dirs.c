/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_dirs.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 00:30:39 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/18 00:19:45 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void	*ft_newdir(char *name)
{
	t_dir	*dir;

	if (!(dir = (t_dir *)malloc(sizeof(t_dir))))
		return (NULL);
	if (!(dir->stat = (t_stat *)malloc(sizeof(t_stat))))
		return (NULL);
	dir->name = name;
	dir->files = NULL;
	dir->dirs = NULL;
	dir->next = NULL;
	return (dir);
}

void	ft_addir(t_ls *env, char *name)
{
	t_dir	*dir;
	t_dir	*tmp;

	dir = ft_newdir(name);
	if (!env->dirs)
		env->dirs = dir;
	else
	{
		tmp = env->dirs;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = dir;
	}
}

void	ft_addir_path(t_dir *dir, char *name)
{
	t_dir	*new;
	t_dir	*tmp;

	new = ft_newdir(name);
	stat(name, new->stat);
	if (!dir->dirs)
		dir->dirs = new;
	else
	{
		tmp = dir->dirs;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}
