/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stat.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 04:01:35 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/19 01:42:59 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include <pwd.h>
#include <grp.h>
#include <time.h>

void	print_file_type(mode_t st_mode)
{
	if ((S_ISFIFO(st_mode)))
		ft_printt("p");
	else if ((S_ISCHR(st_mode)))
		ft_printt("c");
	else if ((S_ISDIR(st_mode)))
		ft_printt("d");
	else if ((S_ISBLK(st_mode)))
		ft_printt("b");
	else if ((S_ISREG(st_mode)))
		ft_printt("-");
	else if ((S_ISLNK(st_mode)))
		ft_printt("l");
	else if ((S_ISSOCK(st_mode)))
		ft_printt("s");
	else
		ft_printt(" ");
}

void	print_file_rights(mode_t st_mode)
{
	char *ret;

	ret = ft_strnew(10);
	ret[0] = ((st_mode & S_IRUSR) ? 'r' : '-');
	ret[1] = ((st_mode & S_IWUSR) ? 'w' : '-');
	ret[2] = ((st_mode & S_IXUSR) ? 'x' : '-');
	ret[2] = ((st_mode & S_ISUID) ? 'S' : ret[2]);
	ret[2] = (ret[2] == 'S' && (st_mode & S_IXUSR) ? 's' : ret[2]);
	ret[3] = ((st_mode & S_IRGRP) ? 'r' : '-');
	ret[4] = ((st_mode & S_IWGRP) ? 'w' : '-');
	ret[5] = ((st_mode & S_IXGRP) ? 'x' : '-');
	ret[5] = ((st_mode & S_ISGID) ? 'S' : ret[5]);
	ret[5] = (ret[5] == 'S' && (st_mode & S_IXGRP) ? 's' : ret[5]);
	ret[6] = ((st_mode & S_IROTH) ? 'r' : '-');
	ret[7] = ((st_mode & S_IWOTH) ? 'w' : '-');
	ret[8] = ((st_mode & S_IXOTH) ? 'x' : '-');
	ret[8] = ((st_mode & S_ISVTX) ? 'T' : ret[8]);
	ret[8] = (ret[8] == 'T' && (st_mode & S_IXOTH) ? 't' : ret[8]);
	ft_printt("%s", ret);
	ft_strdel(&ret);
}

void	print_file_owner(uid_t st_uid, size_t max)
{
	struct passwd	*pass;
	size_t			len;

	if ((pass = getpwuid(st_uid)) != NULL)
	{
		ft_printt("%s  ", pass->pw_name);
		len = ft_strlen(pass->pw_name);
		while ((max - len++) > 0)
			ft_printt(" ");
	}
	else
	{
		ft_printt("%s  ", ft_itoa(st_uid));
		len = ft_nblen(st_uid);
		while ((max - len++) > 0)
			ft_printt(" ");
	}
}

void	print_file_group(gid_t gid, size_t max)
{
	struct group	*grp;
	size_t			len;

	if ((grp = getgrgid(gid)) != NULL)
	{
		ft_printt("%s  ", grp->gr_name);
		len = ft_strlen(grp->gr_name);
		while ((max - len++) > 0)
			ft_printt(" ");
	}
	else
	{
		ft_printt("%s  ", ft_itoa(gid));
		len = ft_nblen(gid);
		while ((max - len++) > 0)
			ft_printt(" ");
	}
}

void	print_file_time(time_t mtime)
{
	char	*tmp;
	time_t	now;

	now = time(0);
	tmp = ft_strdup(ctime(&mtime));
	write(1, tmp + 4, 7);
	if (mtime > now || now - mtime > 15778463)
	{
		ft_printt(" ");
		write(1, tmp + 20, 4);
	}
	else
		write(1, tmp + 11, 5);
	ft_printt(" ");
	ft_strdel(&tmp);
}
