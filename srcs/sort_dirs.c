/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_dirs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 02:07:22 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/16 02:18:26 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void		ft_swap_dirs(t_dir *dir, t_dir *next)
{
	void			*swap;
	struct s_dir	*ft_swap_dirs;
	struct s_file	*swap_files;
	struct stat		*swap_stat;

	swap = dir->name;
	dir->name = next->name;
	next->name = swap;
	ft_swap_dirs = dir->dirs;
	dir->dirs = next->dirs;
	next->dirs = ft_swap_dirs;
	swap_files = dir->files;
	dir->files = next->files;
	next->files = swap_files;
	swap_stat = dir->stat;
	dir->stat = next->stat;
	next->stat = swap_stat;
}

void		ft_sort_dirs_alpha(t_dir *dir)
{
	t_dir		*new;

	new = dir;
	while (new->next != NULL)
	{
		if (ft_strcmp(new->name, new->next->name) > 0)
		{
			ft_swap_dirs(new, new->next);
			new = dir;
			continue ;
		}
		new = new->next;
	}
}

void		ft_sort_dirs_time(t_dir *lst)
{
	t_dir	*tmp;

	tmp = lst;
	while (tmp && tmp->next != NULL)
	{
		if (tmp->stat->st_mtime < tmp->next->stat->st_mtime)
		{
			ft_swap_dirs(tmp, tmp->next);
			tmp = lst;
			continue ;
		}
		tmp = tmp->next;
	}
}

t_dir		*ft_sort_dirs_rev(t_dir *list)
{
	t_dir	*tmp;
	t_dir	*first;
	t_dir	*new;

	tmp = list;
	first = NULL;
	while (tmp)
	{
		new = first;
		first = tmp;
		tmp = tmp->next;
		first->next = new;
	}
	return (first);
}
