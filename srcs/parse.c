/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 22:17:33 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/19 03:29:24 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void	ft_handle_flags(t_ls *env, char *flags)
{
	size_t	i;

	i = 0;
	while (flags[i])
	{
		if (flags[i] == 'l')
			env->l = 1;
		else if (flags[i] == 'R')
			env->up_r = 1;
		else if (flags[i] == 'a')
			env->a = 1;
		else if (flags[i] == 'r')
			env->r = 1;
		else if (flags[i] == 't')
			env->t = 1;
		else
		{
			ft_printt("ft_ls: illegal option -- %c\n", flags[i]);
			ft_free_exit("usage: ft_ls [-lRart] [file ...]", env);
		}
		i++;
	}
}

int		ft_check_type(char *path)
{
	t_stat	*stat;

	if (!(stat = malloc(sizeof(t_stat))))
		return (-1);
	if (lstat(path, stat) == -1)
	{
		ft_putstr_fd("ft_ls: ", 2);
		perror(path);
		free(stat);
		return (-1);
	}
	else if (S_ISDIR(stat->st_mode))
	{
		free(stat);
		return (1);
	}
	else if (S_ISLNK(stat->st_mode))
	{
		free(stat);
		return (2);
	}
	free(stat);
	return (0);
}

void	ft_parse(t_ls *env, int ac, char **av)
{
	int	i;
	int	type;

	i = 0;
	while (++i < ac)
	{
		if (*av[i] == '-' && !env->dirs)
			ft_handle_flags(env, av[i] + 1);
		else
		{
			if (!env->dirs)
				env->path = 1;
			type = ft_check_type(av[i]);
			if (type == 1)
				ft_addir(env, av[i]);
			else if (type == 0)
				ft_addfile(env, av[i]);
			else if (type == 2 && !env->l)
				ft_addir(env, av[i]);
			else if (type == 2)
				ft_addfile(env, av[i]);
		}
	}
	if (!env->dirs && !env->files && type != -1)
		ft_addir(env, ".");
}
