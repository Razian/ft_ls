/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ls.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 03:59:49 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/19 03:45:05 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void		ft_print_file_l(t_ls *env, t_dir *dir, t_file *file, t_format *form)
{
	if (!env->a && is_hidden(env, file->name))
		return ;
	print_file_type(file->stat->st_mode);
	print_file_rights(file->stat->st_mode);
	print_file_nlink(file->stat->st_nlink, form->max_nlink);
	print_file_owner(file->stat->st_uid, form->max_uname);
	print_file_group(file->stat->st_gid, form->max_gname);
	if (S_ISCHR(file->stat->st_mode) || S_ISBLK(file->stat->st_mode))
		print_major_minor(file->stat->st_rdev);
	else if (is_dev(file))
		print_file_size(file->stat->st_size, form->max_size + 3);
	else
		print_file_size(file->stat->st_size, form->max_size);
	print_file_time(file->stat->st_mtime);
	if (!S_ISLNK(file->stat->st_mode))
		ft_printt("%s\n", file->name);
	else
		print_link(dir, file);
}

void		ft_print_dir_l(t_ls *env, t_dir *dir)
{
	t_format	*format;
	t_file		*tmp;
	char		*del;

	format = ft_fill_formats(dir->files);
	if ((ft_strcmp(".", dir->name) != 0 && ft_strcmp("..", dir->name) != 0
				&& !is_path_needed(env, dir->name)) || env->path)
	{
		ft_putendl(del = ft_strjoin(dir->name, ":"));
		ft_strdel(&del);
	}
	if (dir->files != NULL)
		ft_printt("%s %d\n", "total", ft_count_blocks(env, dir));
	while (dir->files)
	{
		tmp = dir->files->next;
		ft_print_file_l(env, dir, dir->files, format);
		ft_strdel(&dir->files->name);
		ft_strdel(&dir->files->path);
		free(dir->files->stat);
		free(dir->files);
		dir->files = tmp;
	}
	ft_printt("\n");
	free(format);
}

void		ft_print_dir(t_ls *env, t_dir *dir)
{
	t_file		*tmp;
	char		*del;

	tmp = dir->files;
	if ((ft_strcmp(dir->name, ".") != 0 && ft_strcmp(dir->name, "..") != 0
				&& !is_path_needed(env, dir->name)) || env->path)
	{
		ft_printt("%s\n", del = ft_strjoin(dir->name, ":"));
		free(del);
	}
	while (tmp != NULL)
	{
		tmp = dir->files->next;
		ft_print_file(env, dir->files);
		ft_strdel(&dir->files->name);
		ft_strdel(&dir->files->path);
		free(dir->files->stat);
		free(dir->files);
		dir->files = tmp;
	}
	ft_printt("\n");
}

void		ft_print_file(t_ls *env, t_file *file)
{
	if (!env->a && is_hidden(env, file->name))
		return ;
	if (!S_ISLNK(file->stat->st_mode))
		ft_printt("%s\n", file->name);
}
