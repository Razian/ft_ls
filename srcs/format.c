/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/18 04:45:19 by tchivert          #+#    #+#             */
/*   Updated: 2019/09/11 23:41:04 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include <pwd.h>
#include <grp.h>

size_t		ft_nblen(unsigned int n)
{
	size_t	i;

	i = 0;
	while (n >= 10)
	{
		n /= 10;
		i++;
	}
	return (i + 1);
}

size_t		max_uname(uid_t st_uid)
{
	struct passwd	*pass;

	if ((pass = getpwuid(st_uid)) != NULL)
		return (ft_strlen(pass->pw_name));
	return (ft_nblen(st_uid));
}

size_t		max_gname(gid_t gid)
{
	struct group	*grp;

	if ((grp = getgrgid(gid)) != NULL)
		return (ft_strlen(grp->gr_name));
	return (ft_nblen(gid));
}

t_format	*ft_fill_formats(t_file *files)
{
	t_format	*format;
	t_file		*tmp;

	tmp = files;
	if (!(format = (t_format *)malloc(sizeof(t_format))))
		return (NULL);
	format->max_nlink = 0;
	format->max_uname = 0;
	format->max_gname = 0;
	format->max_size = 0;
	while (tmp)
	{
		if (format->max_nlink < ft_nblen(tmp->stat->st_nlink))
			format->max_nlink = ft_nblen(tmp->stat->st_nlink);
		if (format->max_uname < max_uname(tmp->stat->st_uid))
			format->max_uname = max_uname(tmp->stat->st_uid);
		if (format->max_gname < max_gname(tmp->stat->st_gid))
			format->max_gname = max_gname(tmp->stat->st_gid);
		if (format->max_size < ft_nblen(tmp->stat->st_size))
			format->max_size = ft_nblen(tmp->stat->st_size);
		tmp = tmp->next;
	}
	return (format);
}
