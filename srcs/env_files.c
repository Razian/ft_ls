/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_files.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 23:37:50 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/16 05:13:53 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

t_file	*ft_newfile(struct dirent *dirp)
{
	t_file	*file;

	if (!(file = (t_file *)malloc(sizeof(t_file))))
		return (NULL);
	file->name = ft_strdup(dirp->d_name);
	if (!(file->stat = (t_stat *)malloc(sizeof(t_stat))))
		return (NULL);
	file->path = NULL;
	file->next = NULL;
	return (file);
}

void	ft_pushfile(t_ls *env, t_file *file)
{
	t_file	*tmp;

	if (env->files == NULL)
		env->files = file;
	else
	{
		tmp = env->files;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = file;
	}
}

void	ft_addfile(t_ls *env, char *path)
{
	t_file	*new;
	size_t	i;
	char	**split;

	if (!(new = malloc(sizeof(t_file))))
		return ;
	split = ft_strsplit(path, '/');
	i = 0;
	while (split[i])
		i++;
	new->name = ft_strdup(split[i - 1]);
	if (!(new->stat = malloc(sizeof(t_stat))))
		return ;
	lstat(path, new->stat);
	new->next = NULL;
	if (ft_strchr(path, '/'))
		new->path = ft_strsub(path, 0, ft_strrchr(path, '/') - path);
	else
		new->path = NULL;
	new->type = NULL;
	ft_pushfile(env, new);
}

void	ft_addfile_from_dir(t_dir *dir, struct dirent *dirp)
{
	t_file	*file;
	t_file	*tmp_file;
	char	*tmp;

	file = ft_newfile(dirp);
	lstat((tmp = ft_strrjoin(dir->name, "/", dirp->d_name)), file->stat);
	ft_strdel(&tmp);
	if (!dir->files)
		dir->files = file;
	else
	{
		tmp_file = dir->files;
		while (tmp_file->next)
			tmp_file = tmp_file->next;
		tmp_file->next = file;
	}
}
