/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 00:50:11 by tchivert          #+#    #+#             */
/*   Updated: 2019/07/19 03:19:29 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

int		is_hidden(t_ls *env, char *name)
{
	if (ft_strcmp(name, ".") == 0)
		return (1);
	else if (ft_strcmp(name, "..") == 0)
		return (1);
	else if (*name == '.' && !env->a)
		return (1);
	else
		return (0);
}

int		is_dev(t_file *files)
{
	t_file	*tmp;

	tmp = files;
	while (tmp->next)
	{
		if (S_ISCHR(tmp->next->stat->st_mode)
				|| S_ISBLK(tmp->next->stat->st_mode))
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

char	*ft_strrjoin(char *s1, char *s2, char *s3)
{
	char	*tmp;
	char	*ret;

	tmp = ft_strjoin(s1, s2);
	ret = ft_strjoin(tmp, s3);
	free(tmp);
	return (ret);
}

char	*get_path(char *path)
{
	char	*res;
	int		ret;

	res = ft_strnew(256);
	ret = readlink(path, res, 256);
	res[ret] = '\0';
	if (path[0] == '/' && res[0] != '/')
		res = ft_strjoin("/", res);
	return (res);
}

int		is_path_needed(t_ls *env, char *name)
{
	t_dir	*tmp;

	tmp = env->dirs;
	while (tmp)
	{
		if (ft_strcmp(tmp->name, name) == 0)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}
