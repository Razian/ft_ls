# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tchivert <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/06/14 01:30:25 by tchivert          #+#    #+#              #
#    Updated: 2019/09/11 23:50:43 by tchivert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls
SRC = srcs/core.c \
	  srcs/parse.c \
	  srcs/handler.c \
	  srcs/env_dirs.c \
	  srcs/env_files.c \
	  srcs/sort_dirs.c \
	  srcs/sort_files.c \
	  srcs/print_ls.c \
	  srcs/stat.c \
	  srcs/stat2.c \
	  srcs/format.c \
	  srcs/utils.c
LIBFT = libft/libft.a
INCLUDES = ./includes
CC = clang -Werror -Wall -Wextra


all: $(NAME)

$(NAME): $(LIBFT) $(SRC)
	@echo "Building $(NAME)..."
	@$(CC) $(SRC) $(LIBFT) -I$(INCLUDES) -o $(NAME)
	@echo "$(NAME) successfully created"

$(LIBFT):
	@make -C libft re

clean:
	@make -C libft clean

fclean: clean
	@/bin/rm -f $(NAME)
	@make -C libft fclean
	@echo "Successfully removed $(NAME)"

re: fclean all
