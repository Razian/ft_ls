/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/18 05:00:24 by tchivert          #+#    #+#             */
/*   Updated: 2019/09/11 23:52:40 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "../libft/libft.h"
# include <stdio.h>
# include <dirent.h>
# include <sys/stat.h>
# include <sys/types.h>

typedef struct stat	t_stat;

typedef struct		s_file
{
	char			*name;
	t_stat			*stat;
	int				*type;
	char			*path;
	struct s_file	*next;
}					t_file;

typedef struct		s_dir
{
	char			*name;
	struct s_dir	*dirs;
	struct s_file	*files;
	t_stat			*stat;
	struct s_dir	*next;
}					t_dir;

typedef struct		s_format
{
	size_t			max_nlink;
	size_t			max_uname;
	size_t			max_gname;
	size_t			max_size;
}					t_format;

typedef struct		s_ls
{
	struct s_dir	*dirs;
	struct s_file	*files;
	int				a;
	int				up_r;
	int				t;
	int				r;
	int				l;
	int				path;
}					t_ls;

int					main(int ac, char **av);
void				ft_free_exit(char *reason, t_ls *env);
void				ft_sort_all(t_ls *env);

void				*ft_newdir(char *name);
void				ft_addir(t_ls *env, char *name);
void				ft_addir_path(t_dir *dir, char *name);

t_file				*ft_newfile(struct dirent *dirp);
void				ft_pushfile(t_ls *env, t_file *file);
void				ft_addfile(t_ls *env, char *path);
void				ft_addfile_from_dir(t_dir *dir, struct dirent *dirp);

void				ft_explore_sub(t_ls *env, t_dir *dir);
void				ft_explore_dir(t_ls *env, t_dir *dir);
void				ft_handle_dir(t_ls *env, t_dir *dir);
void				ft_handle_all(t_ls *env);

void				ft_handle_flags(t_ls *env, char *flags);
int					ft_check_type(char *path);
void				ft_parse(t_ls *env, int ac, char **av);

void				ft_swap_dirs(t_dir *dir, t_dir *next);
void				ft_sort_dirs_alpha(t_dir *dir);
void				ft_sort_dirs_time(t_dir *lst);
t_dir				*ft_sort_dirs_rev(t_dir *list);

void				ft_swap_files(t_file *file, t_file *next_file);
t_file				*ft_sort_files_alpha(t_file *lst);
void				ft_sort_files_time(t_file *lst);
t_file				*ft_sort_files_rev(t_file *file);

int					is_hidden(t_ls *env, char *name);
int					is_dev(t_file *files);
char				*ft_strrjoin(char *s1, char *s2, char *s3);
char				*get_path(char *path);
int					is_path_needed(t_ls *env, char *name);
size_t				ft_nblen(unsigned int n);

void				ft_print_file_l(t_ls *env, t_dir *dir, t_file *file,
		t_format *form);
void				ft_print_dir_l(t_ls *env, t_dir *dir);
void				ft_print_file(t_ls *env, t_file *file);
void				ft_print_dir(t_ls *env, t_dir *dir);

void				print_file_type(mode_t st_mode);
void				print_file_rights(mode_t st_mode);
void				print_file_owner(uid_t st_uid, size_t max);
void				print_file_group(gid_t gid, size_t max);
void				print_file_time(time_t mtime);

void				print_link(t_dir *dir, t_file *file);
void				print_major_minor(dev_t st_rdev);
size_t				ft_count_blocks(t_ls *env, t_dir *dir);
void				print_file_nlink(nlink_t st_nlink, size_t max);
void				print_file_size(off_t st_size, size_t max);

size_t				max_uname(uid_t st_uid);
size_t				max_gname(gid_t gid);
t_format			*ft_fill_formats(t_file *files);

#endif
